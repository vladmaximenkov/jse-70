import marker.ApiCategory;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import ru.vmaksimenkov.tm.model.Task;

import static ru.vmaksimenkov.tm.constant.Const.TASK_URL;

public class TaskTest extends AbstractTest {

    @NotNull
    private final Task task = new Task("test");

    @BeforeClass
    public static void beforeClass() {
        auth();
    }

    private void post(@NotNull final Task task) {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @NotNull final HttpEntity<Task> httpEntity = new HttpEntity<>(task, getHeader());
        restTemplate.postForEntity(TASK_URL, httpEntity, null);
    }

    private void put(@NotNull final Task task) {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @NotNull final HttpEntity<Task> httpEntity = new HttpEntity<>(task, getHeader());
        restTemplate.put(TASK_URL, httpEntity);
    }

    @Nullable
    private Task get(@NotNull final String taskId) {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @NotNull final HttpEntity<Task> httpEntity = new HttpEntity<>(getHeader());
        @NotNull final ResponseEntity<Task> response = restTemplate.exchange(
                TASK_URL + "/" + taskId, HttpMethod.GET, httpEntity, Task.class
        );
        Assert.assertEquals(response.getStatusCode(), (HttpStatus.OK));
        return response.getBody();
    }

    private void delete(@NotNull final String taskId) {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @NotNull final HttpEntity<Task> httpEntity = new HttpEntity<>(getHeader());
        @NotNull final ResponseEntity<Task> response = restTemplate.exchange(
                TASK_URL + "/" + taskId, HttpMethod.DELETE, httpEntity, Task.class
        );
        Assert.assertEquals(response.getStatusCode(), (HttpStatus.OK));
    }

    @Test
    @Category(ApiCategory.class)
    public void postTest() {
        post(task);
        @Nullable final Task taskNew = get(task.getId());
        Assert.assertNotNull(taskNew);
        Assert.assertEquals(task.getId(), taskNew.getId());
    }

    @Test
    @Category(ApiCategory.class)
    public void putTest() {
        post(task);
        task.setName("Put test");
        put(task);
        @Nullable final Task taskNew = get(task.getId());
        Assert.assertNotNull(taskNew);
        Assert.assertEquals("Put test", taskNew.getName());
    }

    @Test
    @Category(ApiCategory.class)
    public void getTest() {
        post(task);
        @Nullable final Task taskNew = get(task.getId());
        Assert.assertNotNull(taskNew);
    }

    @Test
    @Category(ApiCategory.class)
    public void deleteTest() {
        post(task);
        @Nullable Task taskNew = get(task.getId());
        Assert.assertNotNull(taskNew);
        delete(task.getId());
        taskNew = get(task.getId());
        Assert.assertNull(taskNew);
    }


}
