import marker.ApiCategory;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import ru.vmaksimenkov.tm.model.Project;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static ru.vmaksimenkov.tm.constant.Const.PROJECTS_URL;

public class ProjectCollectionTest extends AbstractTest {

    @NotNull
    private final Project project1 = new Project("test1");

    @NotNull
    private final Project project2 = new Project("test2");

    @NotNull
    private final List<Project> projects = new ArrayList<>();

    {
        projects.add(project1);
        projects.add(project2);
    }

    @BeforeClass
    public static void beforeClass() {
        auth();
    }

    private void post(@NotNull final List<Project> projects) {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @NotNull final HttpEntity<List<Project>> httpEntity = new HttpEntity<>(projects, getHeader());
        restTemplate.postForEntity(PROJECTS_URL, httpEntity, Project.class);
    }

    private void put(@NotNull final List<Project> projects) {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @NotNull final HttpEntity<List<Project>> httpEntity = new HttpEntity<>(projects, getHeader());
        restTemplate.put(PROJECTS_URL, httpEntity);
    }

    @Nullable
    private Collection<Project> get() {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @NotNull final HttpEntity<Project> httpEntity = new HttpEntity<>(getHeader());
        @NotNull final ResponseEntity<Collection> response = restTemplate.exchange(
                PROJECTS_URL, HttpMethod.GET, httpEntity, Collection.class
        );
        Assert.assertEquals(response.getStatusCode(), (HttpStatus.OK));
        return response.getBody();
    }

    @Nullable
    private Project deleteAll() {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @NotNull final HttpEntity<Project> httpEntity = new HttpEntity<>(getHeader());
        @NotNull final ResponseEntity<Project> response = restTemplate.exchange(
                PROJECTS_URL, HttpMethod.DELETE, httpEntity, Project.class
        );
        Assert.assertEquals(response.getStatusCode(), (HttpStatus.OK));
        return response.getBody();
    }

    @Test
    @Category(ApiCategory.class)
    public void postTest() {
        deleteAll();
        post(projects);
        @Nullable final Collection<Project> projectResponse = get();
        Assert.assertNotNull(projectResponse);
        Assert.assertEquals(2, projectResponse.size());
    }

    @Test
    @Category(ApiCategory.class)
    public void putTest() {
        project1.setName("Put 1");
        project2.setName("Put 2");
        projects.clear();
        projects.add(project1);
        projects.add(project2);
        put(projects);
        @Nullable final Collection<Project> projectsNew = get();
        Assert.assertNotNull(projectsNew);
        final ArrayList<Object> projects = (ArrayList) projectsNew;
        Assert.assertTrue(projects.get(0).toString().contains("Put 1") || projects.get(0).toString().contains("Put 2"));
    }

    @Test
    @Category(ApiCategory.class)
    public void getTest() {
        post(projects);
        @Nullable final Collection<Project> projectsNew = get();
        Assert.assertNotNull(projectsNew);
    }

    @Test
    @Category(ApiCategory.class)
    public void deleteTest() {
        post(projects);
        deleteAll();
        Assert.assertEquals(0, get().size());
    }

}
