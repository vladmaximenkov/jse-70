import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import ru.vmaksimenkov.tm.model.Result;

import java.net.HttpCookie;
import java.util.ArrayList;
import java.util.List;

import static ru.vmaksimenkov.tm.constant.Const.HOST;

public class AbstractTest {

    @Nullable
    protected static String SESSION_ID = null;

    protected static void auth() {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @NotNull final String url = HOST + "/auth/login?username=test&password=test";
        @NotNull final ResponseEntity<Result> response = restTemplate.getForEntity(url, Result.class);
        Assert.assertEquals(200, response.getStatusCodeValue());
        Assert.assertNotNull(response.getBody());
        Assert.assertTrue(response.getBody().getSuccess());
        @NotNull final HttpHeaders headersResponse = response.getHeaders();
        @NotNull final List<HttpCookie> cookies = java.net.HttpCookie.parse(
                headersResponse.getFirst(HttpHeaders.SET_COOKIE)
        );
        SESSION_ID = cookies.stream()
                .filter(item -> "JSESSIONID".equals(item.getName()))
                .findFirst()
                .get()
                .getValue();
        Assert.assertNotNull(SESSION_ID);
    }

    protected static HttpHeaders getHeader() {
        @NotNull final HttpHeaders headers = new HttpHeaders();
        @NotNull final List<String> cookies = new ArrayList<>();
        cookies.add("JSESSIONID" + "=" + SESSION_ID);
        headers.put(HttpHeaders.COOKIE, cookies);
        headers.setContentType(MediaType.APPLICATION_JSON);
        return headers;
    }

}
