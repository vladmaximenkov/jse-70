import marker.ApiCategory;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import ru.vmaksimenkov.tm.model.Project;

import static ru.vmaksimenkov.tm.constant.Const.PROJECT_URL;

public class ProjectTest extends AbstractTest {

    @NotNull
    private final Project project = new Project("test");

    @BeforeClass
    public static void beforeClass() {
        auth();
    }

    private void post(@NotNull final Project project) {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @NotNull final HttpEntity<Project> httpEntity = new HttpEntity<>(project, getHeader());
        restTemplate.postForEntity(PROJECT_URL, httpEntity, null);
    }

    private void put(@NotNull final Project project) {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @NotNull final HttpEntity<Project> httpEntity = new HttpEntity<>(project, getHeader());
        restTemplate.put(PROJECT_URL, httpEntity);
    }

    @Nullable
    private Project get(@NotNull final String projectId) {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @NotNull final HttpEntity<Project> httpEntity = new HttpEntity<>(getHeader());
        @NotNull final ResponseEntity<Project> response = restTemplate.exchange(
                PROJECT_URL + "/" + projectId, HttpMethod.GET, httpEntity, Project.class
        );
        Assert.assertEquals(response.getStatusCode(), (HttpStatus.OK));
        return response.getBody();
    }

    private void delete(@NotNull final String projectId) {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @NotNull final HttpEntity<Project> httpEntity = new HttpEntity<>(getHeader());
        @NotNull final ResponseEntity<Project> response = restTemplate.exchange(
                PROJECT_URL + "/" + projectId, HttpMethod.DELETE, httpEntity, Project.class
        );
        Assert.assertEquals(response.getStatusCode(), (HttpStatus.OK));
    }

    @Test
    @Category(ApiCategory.class)
    public void postTest() {
        post(project);
        @Nullable final Project projectNew = get(project.getId());
        Assert.assertNotNull(projectNew);
        Assert.assertEquals(project.getId(), projectNew.getId());
    }

    @Test
    @Category(ApiCategory.class)
    public void putTest() {
        post(project);
        project.setName("Put test");
        put(project);
        @Nullable final Project projectNew = get(project.getId());
        Assert.assertNotNull(projectNew);
        Assert.assertEquals("Put test", projectNew.getName());
    }

    @Test
    @Category(ApiCategory.class)
    public void getTest() {
        post(project);
        @Nullable final Project projectNew = get(project.getId());
        Assert.assertNotNull(projectNew);
    }

    @Test
    @Category(ApiCategory.class)
    public void deleteTest() {
        post(project);
        @Nullable Project projectNew = get(project.getId());
        Assert.assertNotNull(projectNew);
        delete(project.getId());
        projectNew = get(project.getId());
        Assert.assertNull(projectNew);
    }


}
