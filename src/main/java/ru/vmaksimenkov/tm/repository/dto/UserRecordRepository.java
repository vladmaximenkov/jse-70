package ru.vmaksimenkov.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.repository.query.Param;
import ru.vmaksimenkov.tm.dto.UserRecord;

public interface UserRecordRepository extends AbstractRecordRepository<UserRecord> {

    @Nullable UserRecord findByLogin(@NotNull @Param("login") String login);

    boolean existsUserRecordByEmail(@NotNull @Param("email") String email);

    boolean existsUserRecordByLogin(@NotNull @Param("login") String login);

}
