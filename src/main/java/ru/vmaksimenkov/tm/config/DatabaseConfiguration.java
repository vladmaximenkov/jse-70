package ru.vmaksimenkov.tm.config;

import com.mchange.v2.c3p0.DriverManagerDataSource;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;
import java.util.Properties;

import static ru.vmaksimenkov.tm.constant.PropertyConst.*;

@Configuration
@EnableTransactionManagement
@ComponentScan("ru.vmaksimenkov.tm")
@PropertySource("classpath:" + FILE_NAME)
@EnableJpaRepositories("ru.vmaksimenkov.tm.repository")
public class DatabaseConfiguration {

    @Bean
    @NotNull
    public DataSource dataSource(
            @NotNull @Value("${" + JDBC_DRIVER_KEY + ":" + JDBC_DRIVER_DEFAULT + "}") final String jdbcDriver,
            @NotNull @Value("${" + JDBC_URL_KEY + ":" + JDBC_URL_DEFAULT + "}") final String jdbcUrl,
            @NotNull @Value("${" + JDBC_USERNAME_KEY + ":" + JDBC_USERNAME_DEFAULT + "}") final String jdbcLogin,
            @NotNull @Value("${" + JDBC_PASSWORD_KEY + ":" + JDBC_PASSWORD_DEFAULT + "}") final String jdbcPassword
    ) {
        @NotNull final DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClass(jdbcDriver);
        dataSource.setJdbcUrl(jdbcUrl);
        dataSource.setUser(jdbcLogin);
        dataSource.setPassword(jdbcPassword);
        return dataSource;
    }

    @Bean
    @NotNull
    public LocalContainerEntityManagerFactoryBean entityManagerFactory(
            @NotNull final DataSource dataSource,
            @Value("${" + JDBC_SHOW_SQL_KEY + ":" + JDBC_SHOW_SQL_DEFAULT + "}") final boolean showSql,
            @NotNull @Value("${" + JDBC_HBM2DDL_KEY + ":" + JDBC_HBM2DDL_DEFAULT + "}") final String tableStrategy,
            @NotNull @Value("${" + JDBC_DIALECT_KEY + ":" + JDBC_DIALECT_DEFAULT + "}") final String dialect
    ) {
        @NotNull final LocalContainerEntityManagerFactoryBean factoryBean =
                new LocalContainerEntityManagerFactoryBean();
        factoryBean.setDataSource(dataSource);
        factoryBean.setJpaVendorAdapter(new HibernateJpaVendorAdapter());
        factoryBean.setPackagesToScan("ru.vmaksimenkov.tm.model", "ru.vmaksimenkov.tm.dto");
        @NotNull final Properties properties = new Properties();
        properties.put("hibernate.show_sql", showSql);
        properties.put("hibernate.hbm2ddl.auto", tableStrategy);
        properties.put("hibernate.dialect", dialect);
        factoryBean.setJpaProperties(properties);
        return factoryBean;
    }

    @Bean
    @NotNull
    public PlatformTransactionManager transactionManager(
            @NotNull final LocalContainerEntityManagerFactoryBean entityManagerFactory
    ) {
        @NotNull final JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(entityManagerFactory.getObject());
        return transactionManager;
    }

}
