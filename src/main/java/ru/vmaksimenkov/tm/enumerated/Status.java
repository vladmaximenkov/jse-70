package ru.vmaksimenkov.tm.enumerated;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vmaksimenkov.tm.exception.StatusNotFoundException;

import static ru.vmaksimenkov.tm.util.ValidationUtil.checkStatus;

@Getter
public enum Status {

    NOT_STARTED("Not started"),
    IN_PROGRESS("In progress"),
    COMPLETE("Complete");

    @NotNull
    private final String displayName;

    Status(@NotNull final String displayName) {
        this.displayName = displayName;
    }

    public static @NotNull Status getStatus(@Nullable String s) {
        if (s == null) throw new StatusNotFoundException();
        s = s.toUpperCase();
        if (!checkStatus(s)) throw new StatusNotFoundException();
        return Status.valueOf(s);
    }

}
