package ru.vmaksimenkov.tm.client;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.web.client.RestTemplate;
import ru.vmaksimenkov.tm.api.resource.IProjectCollectionResource;
import ru.vmaksimenkov.tm.api.resource.IProjectResource;
import ru.vmaksimenkov.tm.dto.ProjectRecord;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import static ru.vmaksimenkov.tm.constant.Const.PROJECTS_URL;
import static ru.vmaksimenkov.tm.constant.Const.PROJECT_URL;

public class ProjectClient implements IProjectResource, IProjectCollectionResource, ProjectClientFeign {

    public void post(@NotNull final List<ProjectRecord> projects) {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        restTemplate.postForEntity(PROJECTS_URL, projects, void.class);
    }

    public @NotNull Collection<ProjectRecord> get() {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @Nullable final ProjectRecord[] projects = restTemplate.getForObject(PROJECTS_URL, ProjectRecord[].class);
        if (projects == null) return new ArrayList<>();
        return Arrays.asList(projects);
    }

    public void put(@NotNull final List<ProjectRecord> projects) {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        restTemplate.put(PROJECTS_URL, projects, void.class);
    }

    public void delete() {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        restTemplate.delete(PROJECTS_URL);
    }

    public void post(@NotNull final ProjectRecord project) {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        restTemplate.postForEntity(PROJECT_URL, project, void.class);
    }

    @Nullable
    public ProjectRecord get(@NotNull final String id) {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        return restTemplate.getForObject(PROJECT_URL + "/{id}", ProjectRecord.class, id);
    }

    public void put(@NotNull final ProjectRecord project) {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        restTemplate.put(PROJECT_URL, project, void.class);
    }

    public void delete(@NotNull final String id) {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        restTemplate.delete(PROJECT_URL + "/{id}", id);
    }

}
