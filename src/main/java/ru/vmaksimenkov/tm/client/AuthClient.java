package ru.vmaksimenkov.tm.client;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.vmaksimenkov.tm.api.resource.IAuthResource;

import javax.xml.namespace.QName;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.Service;
import java.net.URL;

public final class AuthClient {

    @SneakyThrows
    public static IAuthResource getInstance(@NotNull final String baseURL) {
        final String wsdl = baseURL + "/ws/AuthEndpoint?wsdl";
        final URL url = new URL(wsdl);
        final String lp = "AuthEndpointService";
        final String ns = "http://endpoint.tm.vmaksimenkov.ru/";

        final QName qName = new QName(ns, lp);
        final IAuthResource result = Service.create(url, qName).getPort(IAuthResource.class);

        final BindingProvider bindingProvider = (BindingProvider) result;
        bindingProvider.getRequestContext().put(BindingProvider.SESSION_MAINTAIN_PROPERTY, true);

        return result;
    }

    public static void main(String[] args) {
        final IAuthResource authResource = getInstance("http://localhost:8080");
        System.out.println(authResource.login("test", "test").getSuccess());
        System.out.println(authResource.profile().getLogin());
    }

}
