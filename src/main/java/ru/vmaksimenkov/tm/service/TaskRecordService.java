package ru.vmaksimenkov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.vmaksimenkov.tm.dto.TaskRecord;
import ru.vmaksimenkov.tm.repository.dto.TaskRecordRepository;
import ru.vmaksimenkov.tm.repository.dto.UserRecordRepository;

import javax.transaction.Transactional;
import java.util.Collection;
import java.util.List;

@Service
public class TaskRecordService {

    @NotNull
    private final TaskRecordRepository repository;

    @NotNull
    private final UserRecordRepository userRepository;

    @Autowired
    public TaskRecordService(@NotNull final TaskRecordRepository repository, @NotNull final UserRecordRepository userRepository) {
        this.repository = repository;
        this.userRepository = userRepository;
    }

    @Nullable
    public TaskRecord findByUserIdAndId(@NotNull final String userId, @NotNull final String id) {
        return repository.findByUserIdAndId(userId, id);
    }

    @Nullable
    public Collection<TaskRecord> findAll(@NotNull final String userId) {
        return repository.findAllByUserId(userId);
    }

    @Transactional
    public void removeByUserIdAndId(@NotNull final String userId, @NotNull final String id) {
        repository.deleteByUserIdAndId(userId, id);
    }

    @Transactional
    public void removeByUserId(@NotNull final String userId) {
        repository.deleteByUserId(userId);
    }

    @Transactional
    public void merge(@NotNull final String userId, @NotNull final List<TaskRecord> list) {
        list.forEach(repository::save);
    }

    @Transactional
    public void merge(@NotNull final String userId, @NotNull final TaskRecord task) {
        task.setUserId(userId);
        repository.save(task);
    }

    @Transactional
    public void merge(@NotNull final TaskRecord task) {
        repository.save(task);
    }

}
