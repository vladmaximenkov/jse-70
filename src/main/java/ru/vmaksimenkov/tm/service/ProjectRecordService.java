package ru.vmaksimenkov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.vmaksimenkov.tm.dto.ProjectRecord;
import ru.vmaksimenkov.tm.repository.dto.ProjectRecordRepository;
import ru.vmaksimenkov.tm.repository.dto.UserRecordRepository;

import javax.transaction.Transactional;
import java.util.Collection;
import java.util.List;

@Service
public class ProjectRecordService {

    @NotNull
    private final ProjectRecordRepository repository;

    @NotNull
    private final UserRecordRepository userRepository;

    @Autowired
    public ProjectRecordService(@NotNull final ProjectRecordRepository repository, @NotNull final UserRecordRepository userRepository) {
        this.repository = repository;
        this.userRepository = userRepository;
    }

    @Nullable
    public ProjectRecord findByUserIdAndId(@NotNull final String userId, @NotNull final String id) {
        return repository.findByUserIdAndId(userId, id);
    }

    @Nullable
    public Collection<ProjectRecord> findAll(@NotNull final String userId) {
        return repository.findAllByUserId(userId);
    }

    @Transactional
    public void removeByUserIdAndId(@NotNull final String userId, @NotNull final String id) {
        repository.deleteByUserIdAndId(userId, id);
    }

    @Transactional
    public void removeByUserId(@NotNull final String userId) {
        repository.deleteByUserId(userId);
    }

    @Transactional
    public void merge(@NotNull final String userId, @NotNull final List<ProjectRecord> list) {
        list.forEach(repository::save);
    }

    @Transactional
    public void merge(@NotNull final String userId, @NotNull final ProjectRecord project) {
        project.setUserId(userId);
        repository.save(project);
    }

    @Transactional
    public void merge(@NotNull final ProjectRecord project) {
        repository.save(project);
    }

}
