package ru.vmaksimenkov.tm.constant;

import org.jetbrains.annotations.NotNull;

public final class Const {

    @NotNull
    public static final String HOST = "http://localhost:8080";

    @NotNull
    public static final String PROJECT_URL = HOST + "/api/project";

    @NotNull
    public static final String PROJECTS_URL = HOST + "/api/projects";

    @NotNull
    public static final String TASK_URL = HOST + "/api/task";

    @NotNull
    public static final String TASKS_URL = HOST + "/api/tasks";

}
