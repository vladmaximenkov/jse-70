package ru.vmaksimenkov.tm.constant;

import org.apache.activemq.ActiveMQConnection;
import org.jetbrains.annotations.NotNull;

public class PropertyConst {

    @NotNull
    public static final String COMMANDS = "COMMANDS";
    @NotNull
    public static final String COMMANDS_FILE = "./commands.txt";
    @NotNull
    public static final String ERRORS = "ERRORS";
    @NotNull
    public static final String ERRORS_FILE = "./errors.txt";
    @NotNull
    public static final String LOGGER_FILE = "/logger.properties";
    @NotNull
    public static final String MESSAGES = "MESSAGES";
    @NotNull
    public static final String MESSAGES_FILE = "./messages.txt";
    @NotNull
    public static final String BACKUP_XML = "./backup.xml";
    @NotNull
    public static final String FILE_JSON = "./data.json";
    @NotNull
    public final static String PID_FILENAME = "task-manager.pid";
    @NotNull
    public static final String APPLICATION_VERSION_DEFAULT = "1.0";
    @NotNull
    public static final String APPLICATION_VERSION_KEY = "application.version";
    @NotNull
    public static final String BACKUP_INTERVAL_DEFAULT = "5";
    @NotNull
    public static final String BACKUP_INTERVAL_KEY = "backup.interval";
    @NotNull
    public static final String FILE_NAME = "application.properties";
    @NotNull
    public static final String HIBERNATE_CACHEPROVIDER_CONFIG_DEFAULT = "hazelcast.xml";
    @NotNull
    public static final String HIBERNATE_CACHEPROVIDER_CONFIG_KEY = "hibernate.cacheProviderCfg";
    @NotNull
    public static final String HIBERNATE_FACTORY_CLASS_DEFAULT = "com.hazelcast.hibernate.HazelcastLocalCacheRegionFactory";
    @NotNull
    public static final String HIBERNATE_FACTORY_CLASS_KEY = "hibernate.factoryClass";
    @NotNull
    public static final String HIBERNATE_FORMATSQL_DEFAULT = "false";
    @NotNull
    public static final String HIBERNATE_FORMATSQL_KEY = "hibernate.formatSQL";
    @NotNull
    public static final String HIBERNATE_LITEMEMBER_DEFAULT = "false";
    @NotNull
    public static final String HIBERNATE_LITEMEMBER_KEY = "hibernate.cache.hazelcast.use_lite_member";
    @NotNull
    public static final String HIBERNATE_MINPUTS_DEFAULT = "false";
    @NotNull
    public static final String HIBERNATE_MINPUTS_KEY = "hibernate.useMinPuts";
    @NotNull
    public static final String HIBERNATE_QCACHE_DEFAULT = "false";
    @NotNull
    public static final String HIBERNATE_QCACHE_KEY = "hibernate.useQueryCache";
    @NotNull
    public static final String HIBERNATE_REGPREFIX_DEFAULT = "task-manager";
    @NotNull
    public static final String HIBERNATE_REGPREFIX_KEY = "hibernate.regionPrefix";
    @NotNull
    public static final String HIBERNATE_SLCACHE_DEFAULT = "false";
    @NotNull
    public static final String HIBERNATE_SLCACHE_KEY = "hibernate.useSecondLevelCache";
    @NotNull
    public static final String JDBC_DIALECT_DEFAULT = "org.hibernate.dialect.HSQLDialect";
    @NotNull
    public static final String JDBC_DIALECT_KEY = "jdbc.dialect";
    @NotNull
    public static final String JDBC_DRIVER_DEFAULT = "org.hsqldb.jdbcDriver";
    @NotNull
    public static final String JDBC_DRIVER_KEY = "jdbc.driver";
    @NotNull
    public static final String JDBC_HBM2DDL_DEFAULT = "create";
    @NotNull
    public static final String JDBC_HBM2DDL_KEY = "jdbc.hbm2ddl";
    @NotNull
    public static final String JDBC_PASSWORD_DEFAULT = "root";
    @NotNull
    public static final String JDBC_PASSWORD_KEY = "jdbc.password";
    @NotNull
    public static final String JDBC_SHOW_SQL_DEFAULT = "false";
    @NotNull
    public static final String JDBC_SHOW_SQL_KEY = "jdbc.showsql";
    @NotNull
    public static final String JDBC_URL_DEFAULT = "jdbc:hsqldb:mem:example";
    @NotNull
    public static final String JDBC_URL_KEY = "jdbc.url";
    @NotNull
    public static final String JDBC_USERNAME_DEFAULT = "root";
    @NotNull
    public static final String JDBC_USERNAME_KEY = "jdbc.username";
    @NotNull
    public static final String PASSWORD_STRENGTH_DEFAULT = "5";
    @NotNull
    public static final String PASSWORD_STRENGTH_KEY = "password.strength";
    @NotNull
    public static final String PASSWORD_SECRET_DEFAULT = "";
    @NotNull
    public static final String PASSWORD_SECRET_KEY = "password.secret";
    @NotNull
    public static final String SCANNER_INTERVAL_DEFAULT = "5";
    @NotNull
    public static final String SCANNER_INTERVAL_KEY = "scanner.interval";
    @NotNull
    public static final String SERVER_HOST_DEFAULT = "localhost";
    @NotNull
    public static final String SERVER_HOST_KEY = "server.host";
    @NotNull
    public static final String SERVER_PORT_DEFAULT = "8080";
    @NotNull
    public static final String SERVER_PORT_KEY = "server.port";
    @NotNull
    public static final String SESSION_ITERATION_DEFAULT = "1";
    @NotNull
    public static final String SESSION_ITERATION_KEY = "session.iteration";
    @NotNull
    public static final String SESSION_SECRET_DEFAULT = "";
    @NotNull
    public static final String SESSION_SECRET_KEY = "session.secret";
    @NotNull
    public static final String URL = ActiveMQConnection.DEFAULT_BROKER_URL;
    @NotNull
    public static final String SUBJECT = "JCG_TOPIC";

}
