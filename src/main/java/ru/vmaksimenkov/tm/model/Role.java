package ru.vmaksimenkov.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.NotNull;
import ru.vmaksimenkov.tm.enumerated.RoleType;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Cacheable
@NoArgsConstructor
@Table(name = "app_role")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Role extends AbstractBusinessEntity {

    @Column
    @Enumerated(EnumType.STRING)
    private RoleType roleType = RoleType.USER;

    @NotNull
    @Override
    public String toString() {
        return roleType.name();
    }

}
