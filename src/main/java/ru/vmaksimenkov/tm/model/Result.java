package ru.vmaksimenkov.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;

@Getter
@NoArgsConstructor
public class Result {

    @NotNull
    public String message = "";
    @NotNull
    private Boolean success = true;

    public Result(@NotNull final Boolean success) {
        this.success = success;
        this.message = "Success";
    }

    public Result(@NotNull final Exception e) {
        success = false;
        message = e.getMessage();
    }
}
