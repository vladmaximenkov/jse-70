package ru.vmaksimenkov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import ru.vmaksimenkov.tm.api.resource.ITaskResource;
import ru.vmaksimenkov.tm.dto.TaskRecord;
import ru.vmaksimenkov.tm.service.TaskRecordService;
import ru.vmaksimenkov.tm.util.UserUtil;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@RestController
@RequestMapping(value = "/api/task", produces = MediaType.APPLICATION_JSON_VALUE)
@WebService(endpointInterface = "ru.vmaksimenkov.tm.api.resource.ITaskResource")
public class TaskEndpoint implements ITaskResource {

    @NotNull
    private final TaskRecordService service;

    @Autowired
    public TaskEndpoint(@NotNull final TaskRecordService service) {
        this.service = service;
    }

    @Nullable
    @Override
    @WebMethod
    @GetMapping("/{id}")
    public TaskRecord get(@NotNull @WebParam(name = "id") @PathVariable("id") final String id) {
        return service.findByUserIdAndId(UserUtil.getUserId(), id);
    }

    @Override
    @WebMethod
    @PostMapping
    public void post(@NotNull @WebParam(name = "task") @RequestBody final TaskRecord task) {
        service.merge(UserUtil.getUserId(), task);
    }

    @Override
    @WebMethod
    @PutMapping
    public void put(@NotNull @WebParam(name = "task") @RequestBody final TaskRecord task) {
        service.merge(UserUtil.getUserId(), task);
    }

    @Override
    @WebMethod
    @DeleteMapping("/{id}")
    public void delete(@NotNull @WebParam(name = "id") @PathVariable("id") final String id) {
        service.removeByUserIdAndId(UserUtil.getUserId(), id);
    }

}
