package ru.vmaksimenkov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.vmaksimenkov.tm.model.Result;
import ru.vmaksimenkov.tm.service.UserService;

import javax.annotation.Resource;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@RestController
@RequestMapping("/auth")
@WebService(endpointInterface = "ru.vmaksimenkov.tm.api.resource.IAuthResource")
public class AuthEndpoint implements ru.vmaksimenkov.tm.api.resource.IAuthResource {

    @Resource
    private AuthenticationManager authenticationManager;

    @Autowired
    private UserService userService;

    @NotNull
    @WebMethod
    @GetMapping(value = "/login", produces = MediaType.APPLICATION_JSON_VALUE)
    public Result login(
            @WebParam(name = "username") @RequestParam("username") final String username,
            @WebParam(name = "password") @RequestParam("password") final String password
    ) {
        try {
            @NotNull final UsernamePasswordAuthenticationToken token =
                    new UsernamePasswordAuthenticationToken(username, password);
            @NotNull final Authentication authentication =
                    authenticationManager.authenticate(token);
            SecurityContextHolder.getContext().setAuthentication(authentication);
            return new Result(authentication.isAuthenticated());
        } catch (@NotNull final Exception e) {
            return new Result(e);
        }
    }

    @NotNull
    @GetMapping(value = "/session", produces = MediaType.APPLICATION_JSON_VALUE)
    public Authentication session() {
        return SecurityContextHolder.getContext().getAuthentication();
    }

    @NotNull
    @GetMapping(value = "/user", produces = MediaType.APPLICATION_JSON_VALUE)
    public User user(@NotNull @AuthenticationPrincipal(errorOnInvalidType = true) final User user) {
        return user;
    }

    @Nullable
    @WebMethod
    @GetMapping(value = "/profile", produces = MediaType.APPLICATION_JSON_VALUE)
    public ru.vmaksimenkov.tm.model.User profile() {
        @NotNull final SecurityContext securityContext = SecurityContextHolder.getContext();
        @NotNull final Authentication authentication = securityContext.getAuthentication();
        @NotNull final String userName = authentication.getName();
        return userService.findByLogin(userName);
    }

    @NotNull
    @WebMethod
    @GetMapping(value = "/logout", produces = MediaType.APPLICATION_JSON_VALUE)
    public Result logout() {
        SecurityContextHolder.getContext().setAuthentication(null);
        return new Result();
    }

}
