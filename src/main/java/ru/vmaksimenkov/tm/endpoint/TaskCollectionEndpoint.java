package ru.vmaksimenkov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import ru.vmaksimenkov.tm.api.resource.ITaskCollectionResource;
import ru.vmaksimenkov.tm.dto.TaskRecord;
import ru.vmaksimenkov.tm.service.TaskRecordService;
import ru.vmaksimenkov.tm.util.UserUtil;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Collection;
import java.util.List;

@RestController
@RequestMapping(value = "/api/tasks", produces = MediaType.APPLICATION_JSON_VALUE)
@WebService(endpointInterface = "ru.vmaksimenkov.tm.api.resource.ITaskCollectionResource")
public class TaskCollectionEndpoint implements ITaskCollectionResource {

    @NotNull
    private final TaskRecordService service;

    @Autowired
    public TaskCollectionEndpoint(@NotNull final TaskRecordService service) {
        this.service = service;
    }

    @NotNull
    @Override
    @WebMethod
    @GetMapping
    public Collection<TaskRecord> get() {
        return service.findAll(UserUtil.getUserId());
    }

    @Override
    @WebMethod
    @PostMapping
    public void post(@NotNull @WebParam(name = "tasks") @RequestBody final List<TaskRecord> tasks) {
        service.merge(UserUtil.getUserId(), tasks);
    }

    @Override
    @WebMethod
    @PutMapping
    public void put(@NotNull @WebParam(name = "tasks") @RequestBody final List<TaskRecord> tasks) {
        service.merge(UserUtil.getUserId(), tasks);
    }

    @Override
    @WebMethod
    @DeleteMapping
    public void delete() {
        service.removeByUserId(UserUtil.getUserId());
    }

}
